import React, { Component } from 'react';
import Hero from '../Hero';
import {Helmet} from "react-helmet";

class About extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div>
                 <Helmet>
                    <meta charSet="utf-8" />
                    <title>Leonadis Marketing : About</title>
                    
                </Helmet>
               <Hero title="About us" />
                
               <section className="content-section spad">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-6 col-md-6">
                                <img src="img/about-img.png" alt="Leonadis Marketing" />
                            </div>
                            <div className="col-lg-6  col-md-6 about-text">
                            <h2>We believe advertising should touch people.</h2>
                                <h5>Inspire people to remember a brand. </h5>
                                <h5>Inspire people to engage.  </h5>
                                <p>After all, if you canʼt move people, your online presence is not working.
We help our clients and developers reach, engage, and acquire their desired users through digital marketing, using machine learning and artificial intelligence. In a world where online is the place to be</p>
                                <p>Leonadis Marketing is the way to go</p>
                                
                                <h5>Leonadis Marketing is a Digital Agency based in Cornwall. We’ve got years of experience in designing, creating, maintaining and refreshing websites.</h5>
                                <p>Whether you’re looking for a smart and simple business website or a responsive e-commerce website with a whole range of bespoke features, we’ve got you covered.</p>
                                <p>We’ll help you identify your core objectives – what you need from your website – and work with you to get the results your business needs, whether that’s increased exposure, better search engine rankings or more sales.</p>
                                
                            </div>
                        </div>
                        
                    </div>
                </section>


                
            </div>

         );
    }
}
 
export default About;