import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {Helmet} from "react-helmet";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div>
              <Helmet>
                <meta charSet="utf-8" />
                <title>Leonadis Marketing | Digital Agency in Cornwall | Web Development in Cornwall | Digital Marketing in Cornwall</title>
                
              </Helmet> 
                


               <section className="hero-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 hero-text">
                                <h3>Inspire <span>people to</span> remember your <span>Brand</span></h3>
                               
                                
                                    <Link to='/requestaquote' className="site-btn sb-gradients mt-2">Get Started</Link>
                                
                            </div>
                            <div className="col-md-6">
                                <img src="img/laptop.png" className="laptop-image" alt="Leonadis Marketing" />
                            </div>
                        </div>
                    </div>
                </section>

                <section className="about-section spad">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 offset-lg-6 about-text">
                                <h2>We believe advertising should touch people.</h2>
                                <h5>Inspire people to remember a brand. </h5>
                                <h5>Inspire people to engage.  </h5>
                                <p>After all, if you canʼt move people, your online presence is not working.
We help our clients and developers reach, engage, and acquire their desired users through digital marketing, using machine learning and artificial intelligence. In a world where online is the place to be</p>
                                <p>Leonadis Marketing is the way to go</p>
                                <Link to="/about" className="site-btn sb-gradients sbg-line mt-5">Read More</Link>
                            </div>
                        </div>
                        <div className="about-img">
                            <img src="img/about-img.png" alt="Leonadis Marketing" />
                        </div>
                    </div>
                </section>

                <section className="features-section spad gradient-bg">
                    <div className="container text-white">
                        <div className="section-title text-center">
                            <h2>Our Services</h2>
                            <p>We offer best quality services to our clients</p>
                        </div>
                        <div className="row">
                        
                            <div className="col-md-6 col-lg-4 feature">
                               
                                <div className="feature-content">
                                <img src="img/icons/web_design.png" alt="Web Designing in Cornwall" />
                                    <h4 className="mt-2">Web Design</h4>
                                    <p>Your website has got to look good, that’s a given. That's why we strive for the best where quality and user experience is concerned.</p>
                                   
                                </div>
                            </div>
                            
                            <div className="col-md-6 col-lg-4 feature">
                            <div className="feature-content">
                              <img src="img/icons/web_development.png" alt="Web Development in Cornwall" />
                                    <h4 className="mt-2">Web Development</h4>
                                    
                                    <p>If content is what fuels the web, then development must be the engine that drives it. Done right and you shouldn't even notice it is there.</p>
                                   
                                </div>
                            </div>
                            
                            <div className="col-md-6 col-lg-4 feature">
                            <div className="feature-content">
                              <img src="img/icons/seo.png" alt="SEO services in Cornwall" />
                                    <h4 className="mt-2">SEO</h4>
                                    
                                    <p>Combing the essential elements of SEO allows us to support your business goals in the most appropriate way.</p>
                                   
                                </div>
                            </div>
                            
                            <div className="col-md-6 col-lg-4 feature">
                            <div className="feature-content">
                              <img src="img/icons/digital_marketing.png" alt="Digital Marketing services in Cornwall" />
                                    <h4 className="mt-2">Digital Marketing</h4>
                                    
                                    <p>We help ambitious brands to stand out, grow, and forge meaningful connections with their customers and employees alike. </p>
                                    
                                </div>
                            </div>
                            
                            <div className="col-md-6 col-lg-4 feature">
                            <div className="feature-content">
                              <img src="img/icons/ecommerce-icon.png" alt="Ecommerce Website Solutions in Cornwall" />
                                    <h4 className="mt-2">Ecommerce</h4>
                                    
                                    <p>Whether you’re looking to sell to a local, national or international audience, we can help get your products and services noticed</p>
                                    
                                </div>
                            </div>
                            
                            <div className="col-md-6 col-lg-4 feature">
                            <div className="feature-content">
                              <img src="img/icons/website_maintenance.png" alt="Webiste Maintenance Services in Cornwall" />
                                    <h4 className="mt-2">Webiste Maintenance</h4>
                                    
                                    <p>Ongoing updates from a team who care and understand your business and website. We're in this for the long haul.</p>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="process-section spad">
                    <div className="container">
                        <div className="section-title text-center">
                            <h2>The Process</h2>
                            <p>Take the time to understand how we work and see whether we're a good fit for you.</p>
                        </div>
                        <div className="row">
                            <div className="col-md-2 process">
                                <div className="process-step">
                                    <figure className="process-icon">
                                        <img src="img/process-icons/1.png" alt="#" />
                                    </figure>
                                    <h4>The details</h4>
                                    <p>It’s important that we talk and get to know you and your business.</p>
                                </div>
                            </div>
                            <div className="col-md-2 process">
                                <div className="process-step">
                                    <figure className="process-icon">
                                        <img src="img/process-icons/2.png" alt="#" />
                                    </figure>
                                    <h4>The plan</h4> <br/>
                                    <p>It’s time for planning, we’ll start by identifying the pages you will need.</p>
                                </div>
                            </div>
                            <div className="col-md-2 process">
                                <div className="process-step">
                                    <figure className="process-icon">
                                        <img src="img/process-icons/3.png" alt="#" />
                                    </figure>
                                    <h4>The design</h4>
                                    <p>We’ll get our creative ideas flowing and produce a mockup of your new website </p>
                                </div>
                            </div>
                            <div className="col-md-2 process">
                                <div className="process-step">
                                    <figure className="process-icon">
                                        <img src="img/process-icons/4.png" alt="#" />
                                    </figure>
                                    <h4>The build</h4>
                                    <p>We’ll build a working system that matches the final designs.</p>
                                </div>
                            </div>
                            <div className="col-md-2 process">
                                <div className="process-step">
                                    <figure className="process-icon">
                                        <img src="img/process-icons/5.png" alt="#" />
                                    </figure>
                                    <h4>Testing & delivery</h4>
                                    <p>to make sure it works and any issues are under control & deliver to the  Client.</p>
                                </div>
                            </div>
                            <div className="col-md-2 process">
                                <div className="process-step">
                                    <figure className="process-icon">
                                        <img src="img/process-icons/6.png" alt="#" />
                                    </figure>
                                    <h4>Let's stick together</h4>
                                    <p>So we’re here when you need to make improvements or add new functionality. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="features-section spad gradient-bg">
                    <div className="container text-white">
                        <div className="section-title text-center">
                            <h2>Our Work</h2>
                            
                        </div>
                        <div className="row">
                        
                            <div className="col-md-4 col-lg-4 col-sm-12 feature">
                                <a target="_blank" href="https://www.in-marbella.com/">
                                <div className="feature-content">
                                <img src="img/our_work/inmarbella.jpg" alt="In Marbella" />
                                    <h4 className="mt-2">In Marbella</h4>
                                    
                                   
                                </div>
                                </a>
                            </div>
                            
                            <div className="col-md-4 col-lg-4 col-sm-12 feature">
                            <a target="_blank" href="http://www.tonehouseltd.co.uk/">
                                <div className="feature-content">
                                <img src="img/our_work/tonehouseltd.jpg" alt="Tone House" />
                                    <h4 className="mt-2">Tone House</h4>
                                    
                                   
                                </div>
                                </a>
                            </div>
                            
                            <div className="col-md-4 col-lg-4 col-sm-12 feature">
                            <a target="_blank" href="http://www.anglo-tantalum.com/">
                                <div className="feature-content">
                                <img src="img/our_work/anglo-tantalum.jpg" alt="Anglo Tantalum" />
                                    <h4 className="mt-2">Anglo Tantalum</h4>
                                    
                                   
                                </div>
                                </a>
                            </div>
                            
                            
                            
                            
                            
                           
                        </div>
                    </div>
                </section>

            

        

                
            </div>

         );
    }
}
 
export default Home;