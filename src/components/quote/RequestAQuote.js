import React, { Component } from 'react';
import Hero from '../Hero';
import {Helmet} from "react-helmet";

class RequestAQuote extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div>
               <Helmet>
                    <meta charSet="utf-8" />
                    <title>Leonadis Marketing : Request a Quote</title>
                    
                </Helmet>
              <Hero title="Request A Quote" />  
             
              <section className="content-section spad">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-12 col-md-12">
                            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfLMwn01OPi4t9Iy0flbRNwt3kV6m5GuGHtRLNUaMgp7V6TqA/viewform?embedded=true" width="100%" height="1110" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
                            </div>
                            
                        </div>
                        
                    </div>
                </section>


                
            </div>

         );
    }
}
 
export default RequestAQuote;