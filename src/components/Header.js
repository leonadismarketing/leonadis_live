import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (
            <header className="header-section clearfix">
                    <div className="container-fluid">
                        <Link to="/" className="site-logo">
                            <img src="img/logo.png" alt="" />
                        </Link>
                        <div className="responsive-bar"><i className="fa fa-bars"></i></div>
                       
                        <Link to="/requestaquote" className="site-btn">Request a Quote</Link>
                        <nav className="main-menu">
                            <ul className="menu-list">
                                <li><Link to="/">Home</Link></li>
                                <li><Link to="/about">About</Link></li>
                                <li><Link to="/services">Services</Link></li>
                                <li><Link to="/work">Work</Link></li>
                                <li><Link to="/contact">Contact</Link></li>
                            </ul>
                        </nav>
                    </div>
               </header>
         );
    }
}
 
export default Header;