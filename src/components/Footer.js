import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (
            <footer className="footer-section">
		<div className="container">
			<div className="row spad">
				<div className="col-md-6 col-lg-3 footer-widget">
					<img src="img/logo.png" className="mb-4" alt="" />
					<p>Leonadis Marketing is a Digital Agency based in Cornwall. We’ve got years of experience in designing, creating, maintaining and refreshing websites.</p>
					
				</div>
				<div className="col-md-6 col-lg-2 offset-lg-1 footer-widget">
					<h5 className="widget-title">Links</h5>
					<ul>
						<li><Link to="/">Home</Link></li>
						<li><Link to="/about">About</Link></li>
						<li><Link to="/services">Services</Link></li>
						<li><Link to="/work">Work</Link></li>
						<li><Link to="/career">Career</Link></li>
						<li><Link to="/contact">Contact</Link></li>
						<li><Link to="/requestaquote">Request A Quote</Link></li>
						<li><a href="#" target="_blank">Blog</a></li>
					</ul>
				</div>
				<div className="col-md-6 col-lg-2 offset-lg-1 footer-widget">
				<h5 className="widget-title">Contact Us</h5>
					<h5>Leonadis Marketing</h5>
					<p>
						Unit 8 Prow Park <br/>
						Treloggan industrial estate <br/>
						Newquay <br/>
						Cornwall <br/>
						TR7 2SX</p>
				    <p><strong>Phone:</strong> +44 7931 467967</p>
					<p><strong>Email:</strong> info@leonadismarketing.com <br/> leonadismarketing@gmail.com</p>	
				</div>
				<div className="col-md-6 col-lg-3 footer-widget pl-lg-5 pl-3">
				    	
					<h5 className="widget-title">Follow Us</h5>
					<div className="social">
						<a href="https://www.facebook.com/LeonadisMarketing/" className="facebook" target="_blank"><i className="fa fa-facebook"></i></a>
						
						<a href="https://www.instagram.com/leonadismarketing/" className="instagram" target="_blank"><i className="fa fa-instagram"></i></a>
						<a href="https://twitter.com/LeonadisAgency" className="twitter" target="_blank"><i className="fa fa-twitter"></i></a>
					</div>
				</div>
			</div>
			<div className="footer-bottom">
				<div className="row">
					<div className="col-lg-12 store-links text-center text-lg-left pb-3 pb-lg-0">
					Copyright &copy; Leonadis Marketing 2019. All rights reserved 
					</div>
					
				</div>
			</div>
		</div>
	</footer>
         );
    }
}
 
export default Footer;