import React, { Component } from 'react';
import Hero from '../Hero';
import {Helmet} from "react-helmet";

class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div>
              <Helmet>
                    <meta charSet="utf-8" />
                    <title>Leonadis Marketing : Contact</title>
                    
                </Helmet> 
              <Hero title="Contact" />  
              
              <section className="content-section spad">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-8 col-md-8">
                            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfuIIa3fLDm3s97veFrmt_c56C_sPIaf4ySmUpBrjbXNErMGg/viewform?embedded=true" width="100%" height="850" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
                            </div>
                            <div className="col-lg-4  col-md-4 about-text">
                            <h4>Leonadis Marketing</h4>
					<p>
						Unit 8 Prow Park <br/>
						Treloggan industrial estate <br/>
						Newquay <br/>
						Cornwall <br/>
						TR7 2SX</p>
				    <p><strong>Phone:</strong> +44 7931 467967</p>
					<p><strong>Email 1:</strong> info@leonadismarketing.com</p>	
                    <p><strong>Email 2:</strong> leonadismarketing@gmail.com</p>	
                                
                            </div>
                        </div>
                        
                    </div>
                </section>


                
            </div>

         );
    }
}
 
export default Contact;