import React, { Component } from 'react';
import Hero from '../Hero';
import {Helmet} from "react-helmet";

class Services extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div>
              <Helmet>
                    <meta charSet="utf-8" />
                    <title>Leonadis Marketing : Services</title>
                    
                </Helmet> 
              <Hero title="Services" />
                
              <section className="features-section spad gradient-bg">
                    <div className="container text-white">
                        <div className="section-title text-center">
                           
                            <h3>We offer best quality services to our clients</h3>
                        </div>
                        <div className="row">
                        
                            <div className="col-md-6 col-lg-4 feature">
                               
                                <div className="feature-content">
                                <img src="img/icons/web_design.png" alt="Web Designing in Manchester" />
                                    <h4 className="mt-2">Web Design</h4>
                                    <p>Your website has got to look good, that’s a given. That's why we strive for the best where quality and user experience is concerned.</p>
                                   
                                </div>
                            </div>
                            
                            <div className="col-md-6 col-lg-4 feature">
                            <div className="feature-content">
                              <img src="img/icons/web_development.png" alt="Web Development in Manchester" />
                                    <h4 className="mt-2">Web Development</h4>
                                    
                                    <p>If content is what fuels the web, then development must be the engine that drives it. Done right and you shouldn't even notice it is there.</p>
                                   
                                </div>
                            </div>
                            
                            <div className="col-md-6 col-lg-4 feature">
                            <div className="feature-content">
                              <img src="img/icons/seo.png" alt="SEO services in Manchester" />
                                    <h4 className="mt-2">SEO</h4>
                                    
                                    <p>Combing the essential elements of SEO allows us to support your business goals in the most appropriate way.</p>
                                   
                                </div>
                            </div>
                            
                            <div className="col-md-6 col-lg-4 feature">
                            <div className="feature-content">
                              <img src="img/icons/digital_marketing.png" alt="Digital Marketing services in Manchester" />
                                    <h4 className="mt-2">Digital Marketing</h4>
                                    
                                    <p>We help ambitious brands to stand out, grow, and forge meaningful connections with their customers and employees alike. </p>
                                    
                                </div>
                            </div>
                            
                            <div className="col-md-6 col-lg-4 feature">
                            <div className="feature-content">
                              <img src="img/icons/ecommerce-icon.png" alt="Ecommerce Website Solutions in Manchester" />
                                    <h4 className="mt-2">Ecommerce</h4>
                                    
                                    <p>Whether you’re looking to sell to a local, national or international audience, we can help get your products and services noticed</p>
                                    
                                </div>
                            </div>
                            
                            <div className="col-md-6 col-lg-4 feature">
                            <div className="feature-content">
                              <img src="img/icons/website_maintenance.png" alt="Webiste Maintenance Services in Manchester" />
                                    <h4 className="mt-2">Webiste Maintenance</h4>
                                    
                                    <p>Ongoing updates from a team who care and understand your business and website. We're in this for the long haul.</p>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                
            </div>

         );
    }
}
 
export default Services;