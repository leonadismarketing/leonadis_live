import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Hero extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            title: this.props.title
         }
    }
    render() { 
        return (

            <section className="page-info-section">
                <div className="container">
                    <h2 className="mt-5">{this.state.title}</h2>
                    <div className="site-beradcamb">
                        <Link to="/">Home</Link>
                        <span><i className="fa fa-angle-right"></i> {this.state.title}</span>
                    </div>
                </div>
            </section>
         );
    }
}
 
export default Hero;