import React, { Component } from 'react';
import Hero from '../Hero';
import {Helmet} from "react-helmet";

class Work extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div>
               <Helmet>
                    <meta charSet="utf-8" />
                    <title>Leonadis Marketing : Work</title>
                    
                </Helmet>
               <Hero title="Work" />
                
               <section className="features-section spad gradient-bg">
                    <div className="container text-white">
                        <div className="section-title text-center">
                            <h2>Our Work</h2>
                            
                        </div>
                        <div className="row">
                        
                            <div className="col-md-4 col-lg-4 col-sm-12 feature">
                                <a target="_blank" href="https://www.in-marbella.com/">
                                <div className="feature-content">
                                <img src="img/our_work/inmarbella.jpg" alt="In Marbella" />
                                    <h4 className="mt-2">In Marbella</h4>
                                    
                                   
                                </div>
                                </a>
                            </div>
                            
                            <div className="col-md-4 col-lg-4 col-sm-12 feature">
                            <a target="_blank" href="http://www.tonehouseltd.co.uk/">
                                <div className="feature-content">
                                <img src="img/our_work/tonehouseltd.jpg" alt="Tone House" />
                                    <h4 className="mt-2">Tone House</h4>
                                    
                                   
                                </div>
                                </a>
                            </div>
                            
                            <div className="col-md-4 col-lg-4 col-sm-12 feature">
                            <a target="_blank" href="http://www.anglo-tantalum.com/">
                                <div className="feature-content">
                                <img src="img/our_work/anglo-tantalum.jpg" alt="Anglo Tantalum" />
                                    <h4 className="mt-2">Anglo Tantalum</h4>
                                    
                                   
                                </div>
                                </a>
                            </div>
                            
                            
                            
                            
                            
                           
                        </div>
                    </div>
                </section>


                
            </div>

         );
    }
}
 
export default Work;