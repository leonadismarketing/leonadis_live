import React from 'react';
import { HashRouter as Router, Route, Switch} from 'react-router-dom';
import Header from './components/Header';
import Footer from './components/Footer';


import Home from './components/home/Home';
import About from './components/about/About';
import Services from './components/services/Services';
import Work from './components/work/Work';
import Career from './components/career/Career';
import Contact from './components/contact/Contact';
import RequestAQuote from './components/quote/RequestAQuote';

function App() {
  return (
    <Router>
    <div>
       <Header />
          <Route exact path="/" component={Home} />
          <Switch>
          <Route exact path="/about" component={About} />
          </Switch>
          <Switch>
          <Route exact path="/services" component={Services} />
          </Switch>
          <Switch>
          <Route exact path="/work" component={Work} />
          </Switch>
          <Switch>
          <Route exact path="/career" component={Career} />
          </Switch>
          <Switch>
          
          <Route exact path="/contact" component={Contact} />
          </Switch>
          <Switch>
          <Route exact path="/requestaquote" component={RequestAQuote} />
          </Switch>
       <Footer />

    </div>
    </Router>
  );
}

export default App;
